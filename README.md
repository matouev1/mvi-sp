# Predikce genů pomocí konvoluční neuronové sítě
Eva Matoušková


### Úvod do problematiky
Ve své semestrální práci jsem se zaměřila na predikci genů ze sekvence DNA. Deoxyribonukleová kyselina (DNA) obsahuje sekvenci čtyř bází: adeninu (A), guaninu (G), cytosinu (C) a thyminu (T). Základní jednotkou dědičnosti je gen, který obsahuje tři typy sekvencí: regulační sekvence, introny a exony. Exony jsou protein kódující sekvence a ty jsem ve své práci určovala pomocí konvoluční sítě.
Ve své seminární práci jsem se rozhodla, že model natrénuji na sekvencích z myši, kuřete, prasete, psa a šimpanze. Tato zvířata jsem vybrala na základě fylogenetických stromů příbuznosti organismů.


### Stažení dat k výpočtu
V následujícím seznamu jsou odkazy na stránky s DNA sekvencemi a anotacemi pro jednotlivé organismy. Ty je potřeba stáhnout do složky Data/"anglický název organismu" a rozbalit v tomto adresáři.
- DNA sekvence jsou v "Download DNA sequence (FASTA)"
- anotace ke genomu v "Download GTF or GFF3 files for genes, cDNAs, ncRNA, proteins"
    - formát GFF3 
- Human: http://www.ensembl.org/Homo_sapiens/Info/Index
    - chromozom 13 
- Mouse: http://www.ensembl.org/Mus_musculus/Info/Index
    - chromozom 1-19
- Dog: http://www.ensembl.org/Canis_familiaris/Info/Index
    - chromozom 13-15
- Chimpanzee: http://www.ensembl.org/Pan_troglodytes/Info/Index
    - chromozom 4-6
- Chicken: http://www.ensembl.org/Gallus_gallus/Info/Index
    - chromozom 10-12
- Pig: http://www.ensembl.org/Sus_scrofa/Info/Index
    - chromozom 7-9

### Puštění skriptů
1. extract_exon_intron ... slouží k předzpracování dat a uložení matic
2. mouse_generate_parameters ... vygeneruje sady parametrů 
3. mouse_CNN_optimalization ... optimalizace parametrů na myším genomu
4. mouse_optim_res_display ... zobrazení výsledků z optimalizace
3+4 předpřipraveno pro vynechání chybových sad
5. CNN_mouse_human ... kompletní model pro trénování na myším genomu i otestování na lidských sekvencích 
6. CNN_animals_human ... model pro trénování na směsi zvířecích genomů (5 obratlovců) i testování na lidských sekvencích


### Extra soubory param_index a param_values
Tyto soubory obsahují parametry, které jsem vygenerovala a použila při optimalizaci.
- param_index ... indexy parametrů
- param_values ... konkrétní hodnoty použité pro optimalizaci
